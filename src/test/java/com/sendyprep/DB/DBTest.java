package com.sendyprep.DB;
import java.sql.SQLException;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
public class DBTest {
    @Test
    public void givenConnection_thenFetchDbData() throws SQLException {
        List<Rider> riders = DB.fetchData();
        assertEquals( 101, riders.size() );
    }
}
