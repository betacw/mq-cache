package com.sendyprep.MQTT;

public class RiderUpdate
{
    private String license_status;

    private String speed;

    private String exclusive_status;

    private String altitude;

    private String busy;

    private String lng;

    private String partner_level;

    private String city_id;

    private String course;

    private String time;

    private String sim_card_sn;

    private String name;

    private String phone_no;

    private String active;

    private String bearing;

    private String rating;

    private String rider_id;

    private String accuracy;

    private String vendor_type;

    private String carrier_type;

    private String deviceId;

    private String lat;

    public String getLicense_status ()
    {
        return license_status;
    }

    public void setLicense_status (String license_status)
    {
        this.license_status = license_status;
    }

    public String getSpeed ()
    {
        return speed;
    }

    public void setSpeed (String speed)
    {
        this.speed = speed;
    }

    public String getExclusive_status ()
    {
        return exclusive_status;
    }

    public void setExclusive_status (String exclusive_status)
    {
        this.exclusive_status = exclusive_status;
    }

    public String getAltitude ()
    {
        return altitude;
    }

    public void setAltitude (String altitude)
    {
        this.altitude = altitude;
    }

    public String getBusy ()
    {
        return busy;
    }

    public void setBusy (String busy)
    {
        this.busy = busy;
    }

    public String getLng ()
    {
        return lng;
    }

    public void setLng (String lng)
    {
        this.lng = lng;
    }

    public String getPartner_level ()
    {
        return partner_level;
    }

    public void setPartner_level (String partner_level)
    {
        this.partner_level = partner_level;
    }

    public String getCity_id ()
    {
        return city_id;
    }

    public void setCity_id (String city_id)
    {
        this.city_id = city_id;
    }

    public String getCourse ()
    {
        return course;
    }

    public void setCourse (String course)
    {
        this.course = course;
    }

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public String getSim_card_sn ()
    {
        return sim_card_sn;
    }

    public void setSim_card_sn (String sim_card_sn)
    {
        this.sim_card_sn = sim_card_sn;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getPhone_no ()
    {
        return phone_no;
    }

    public void setPhone_no (String phone_no)
    {
        this.phone_no = phone_no;
    }

    public String getActive ()
    {
        return active;
    }

    public void setActive (String active)
    {
        this.active = active;
    }

    public String getBearing ()
    {
        return bearing;
    }

    public void setBearing (String bearing)
    {
        this.bearing = bearing;
    }

    public String getRating ()
    {
        return rating;
    }

    public void setRating (String rating)
    {
        this.rating = rating;
    }

    public String getRider_id ()
    {
        return rider_id;
    }

    public void setRider_id (String rider_id)
    {
        this.rider_id = rider_id;
    }

    public String getAccuracy ()
    {
        return accuracy;
    }

    public void setAccuracy (String accuracy)
    {
        this.accuracy = accuracy;
    }

    public String getVendor_type ()
    {
        return vendor_type;
    }

    public void setVendor_type (String vendor_type)
    {
        this.vendor_type = vendor_type;
    }

    public String getCarrier_type ()
    {
        return carrier_type;
    }

    public void setCarrier_type (String carrier_type)
    {
        this.carrier_type = carrier_type;
    }

    public String getDeviceId ()
    {
        return deviceId;
    }

    public void setDeviceId (String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [license_status = "+license_status+", speed = "+speed+", exclusive_status = "+exclusive_status+", altitude = "+altitude+", busy = "+busy+", lng = "+lng+", partner_level = "+partner_level+", city_id = "+city_id+", course = "+course+", time = "+time+", sim_card_sn = "+sim_card_sn+", name = "+name+", phone_no = "+phone_no+", active = "+active+", bearing = "+bearing+", rating = "+rating+", rider_id = "+rider_id+", accuracy = "+accuracy+", vendor_type = "+vendor_type+", carrier_type = "+carrier_type+", deviceId = "+deviceId+", lat = "+lat+"]";
    }
}
