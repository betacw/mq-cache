package com.sendyprep.MQTT;
import java.util.Arrays;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class Practice {

    public static void main(String[] args) {

        String topic        = "partner_app_positions/ke-nairobi/";
        int qos             = 2;
        String broker       = "tcp://chat.sendyit.com:8883";
        String clientId     = "cb";
        MemoryPersistence persistence = new MemoryPersistence();




        try {
            MqttClient client = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setPassword("93a3a43dbac9ddd362702fb525b42a2d".toCharArray());
            connOpts.setUserName("sendy");
            connOpts.setCleanSession(true);
            client.setCallback(new MqttCallback() {

                public void connectionLost(Throwable cause) { //Called when the client lost the connection to the broker
                }

                public void messageArrived(String topic, MqttMessage message) throws Exception {
//                    Cache.getConnection().set(topic,message.toString());
                    JsonParser parser = new JsonParser();
                    JsonElement element = parser.parse(Arrays.toString(message.getPayload()));
                    System.out.println(element);
//                    System.out.println(topic + ": " + Arrays.toString(message.getPayload()));
//                    System.out.println(topic + ": " + message.toString());
                }

                public void deliveryComplete(IMqttDeliveryToken token) {//Called when a outgoing publish is complete
                }
            });


            System.out.println("Connecting to broker: "+broker);
            client.connect(connOpts);
            System.out.println("Connected");
            System.out.printf("Subscribing to %s%n", topic);
            client.subscribe(topic+"#", 1);
//            System.exit(0);
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
    }
}
