package com.sendyprep.DB;

public class Rider {
    private int riderId;
    private String name;
    private String phone;

    public int getRiderId() {
        return riderId;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRiderId(int riderId) {
        this.riderId = riderId;
    }

    @Override
    public String toString() {
        return String.format("[riderId=%d, name=%s, phone=%s]", riderId, name, phone);
    }
}
