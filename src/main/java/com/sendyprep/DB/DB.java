package com.sendyprep.DB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DB {
    public static List<Rider> fetchData(){
        final String SQL_QUERY = "select rider_id, concat(f_name, \" \", s_name) as name, phone_no from rider";
        List<Rider> riders = null;
        try  {
            Connection con = DataSource.getConnection();
            PreparedStatement pst = con.prepareStatement(SQL_QUERY);
            ResultSet rs = pst.executeQuery();
            riders = new ArrayList<Rider>();
            Rider rider;
            while (rs.next()) {
                rider = new Rider();
                rider.setRiderId(rs.getInt("rider_id"));
                rider.setName(rs.getString("name"));
                rider.setPhone(rs.getString("phone_no"));
                riders.add(rider);
            }

        }catch (SQLException e) {
            e.printStackTrace();
        }
        return riders;
    }
    public static void main(String[] args) {
        for (Rider rida : fetchData())
            System.out.println(rida);
    }


}

