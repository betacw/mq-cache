package com.sendyprep.jsonp;

import com.google.gson.annotations.Since;

public class CB {
    @Since(1.0)
    public String name;
    @Since(1.0)
    public String email;
    @Since(1.0)
    public int age;
    @Since(1.0)
    public boolean isDeveloper;
    @Since(1.1)
    public String nickName;
}
