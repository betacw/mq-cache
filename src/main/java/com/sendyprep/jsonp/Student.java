package com.sendyprep.jsonp;

class Student {
    private int rollNo;
    private Name name;

    public int getRollNo() {
        return rollNo;
    }
    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }
    public Name getName() {
        return name;
    }
    public void setName(Name name) {
        this.name = name;
    }
    static class Name {
        public String firstName;
        public String lastName;
    }
}