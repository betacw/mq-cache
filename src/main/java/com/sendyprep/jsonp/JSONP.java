package com.sendyprep.jsonp;
import com.google.gson.*;

public class JSONP {
    public static void main(String[] args) {
        UserSimple userObject = new UserSimple();
        userObject.name ="Clifford";
        userObject.email ="betaclifford@gmail.com";
        userObject.age =23;
        userObject.isDeveloper = true;

        GsonBuilder builder = new GsonBuilder();
        builder.setVersion(1.1);
        Gson gson = builder.create();
        String userJson = gson.toJson(userObject);
        System.out.println(userJson);
        CB cb = gson.fromJson(userJson, CB.class);
        System.out.println(cb.nickName);
        cb.nickName = "betacw";
        if (cb.nickName != null){
            System.out.println(cb.name);
        }
        System.out.println(gson.toJson(cb));



    }
}
