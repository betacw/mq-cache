package com.sendyprep.Cache;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;

public class Cache {
    private static RedisClient client = new RedisClient("localhost");
    private static RedisConnection<String, String> connection = client.connect();
    private Cache(){}
    public static RedisConnection getConnection() {
        return connection;
    }
}
